// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Interface/ABCharacterWidgetInterface.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interface/ABAnimationAttackInterface.h" 
#include "Interface/ABCharacterItemInterface.h"  
#include "GameData/ABCharacterStat.h"
#include "ABCharacterBase.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogABCharacter, Log, All);

DECLARE_DELEGATE_OneParam(FOnTakeItemDelegate, class UABItemData*/*InItemData*/)

USTRUCT(BlueprintType)
struct FTakeItemDelegateWrapper
{
	GENERATED_BODY()
	
	FTakeItemDelegateWrapper() {}
	FTakeItemDelegateWrapper(const FOnTakeItemDelegate& InItemDelegate) : ItemDelegate(InItemDelegate) {};
	
	FOnTakeItemDelegate ItemDelegate;
};


UENUM()
enum class ECharacterControlType : uint8
{
	Shoulder,
	Quater
};
UCLASS()
class ARENABATTLE_API AABCharacterBase : public ACharacter, public IABAnimationAttackInterface, public IABCharacterWidgetInterface, public IABCharacterItemInterface
{
	GENERATED_BODY()
	
public:
	// Sets default values for this character's properties
	AABCharacterBase();

	virtual void PostInitializeComponents() override;

	//To override AABCharacterPlayer  
	//In this place, task about "Pawn & movement" control data setting 
	virtual void SetCharacterControlData(const class UABCharacterControlData* CharacterControlData);


	//create "control data manager" data structure 
	UPROPERTY(EditAnywhere, Category =CharacterControl, Meta = (AllowPrivateAccess = "true"))
	TMap<ECharacterControlType, class UABCharacterControlData*> CharacterControlManager;


//Combo Action Section
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
	TObjectPtr<class UAnimMontage> ComboActionMontage; 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UABComboActionData> ComboActionData;


	void ProcessComboCommand();
	 
	void ComboActionBegin();
	void ComboActionEnd(class UAnimMontage* TargetMontage,bool IsProperlyEnded );
	virtual void NotifyComboActionEnd();
	void SetComboCheckTimer();
	void ComboCheck();

	//This is Inner Value. So, not Clairfy UPROPERTY
	// 
	//if CurrentCombo == 0, It means Not Playing Montage 
	int32 CurrentCombo = 0;

	FTimerHandle ComboTimerHandle;
	bool HasNextComboCommand = false;



// Attack Hit Section
protected:
	virtual void AttackHitCheck() override; 
	virtual float TakeDamage(float DamageTaken, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override; 


//Dead Section
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
	TObjectPtr<class UAnimMontage> DeadMontage;

	virtual void SetDead();
	void PlayDeadAnimation();

	float DeadEventDelayTime = 5.0f; 

// Stat Section
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stat, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UABCharacterStatComponent> Stat;

//UI Widget Section
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UABWidgetComponent> HpBar;


	virtual void SetupCharacterWidget(class UABUserWidget* InUserwidget) override;




//Item Section
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Equipment, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class USkeletalMeshComponent> Weapon;


	UPROPERTY()
	TArray<FTakeItemDelegateWrapper> TakeItemActions;
	virtual void TakeItem(class UABItemData* InItemData) override;  
	virtual void DrinkPotion(class UABItemData* InItemData);
	virtual void EquipWeapon(class UABItemData* InItemData);
	virtual void ReadScroll(class UABItemData* InItemData);


//Stat Section
public:
	int32 GetLevel();
	void SetLevel(int32 InNewLevel);
	void ApplyStat(const FABCharacterStat& BaseStat, const FABCharacterStat& ModifierStat); 
}; 

