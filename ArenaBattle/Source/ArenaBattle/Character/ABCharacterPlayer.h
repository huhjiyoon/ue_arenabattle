// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h" 
#include "InputActionValue.h"
#include "Character/ABCharacterBase.h"
#include "Interface/ABCharacterHUDInterface.h"  
#include "ABCharacterPlayer.generated.h"

/*
 실제 playercharacter로써 사용할 클래스이기 때문에 카메라 부착 필요
 */

UCLASS()
class ARENABATTLE_API AABCharacterPlayer : public AABCharacterBase, public IABCharacterHUDInterface
{
	GENERATED_BODY()

public:
	AABCharacterPlayer();

protected:
	virtual void BeginPlay() override;
	virtual void SetDead() override;
public:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
//Character Control Section
protected:
	void ChangeCharacterControl();
	void SetCharacterControl(ECharacterControlType NewCharacterControlType); 
    //In this place, task about "Camera & SpringArm" control data setting 
	virtual void SetCharacterControlData(const class UABCharacterControlData* CharacterControlData) override; 

//Camera Section 
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class USpringArmComponent> CameraBoom; 

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UCameraComponent> FollowCamera;

//Input Section 
protected:

	//InputAction
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> ChangeControlAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> ShoulderMoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> ShoulderLookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> QuaterMoveAction;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UInputAction> AttackAction; 

	//Functions are associate with InputAction(MoveAction & LookAction)
	void ShoulderMove(const FInputActionValue& Value);
	void ShoulderLook(const FInputActionValue& Value);   

	void QuaterMove(const FInputActionValue& Value);

	//This Function is related with Animation Montage
	void Attack(const FInputActionValue& Value);

	//Check CurrentCharacterControlType Use ENUM
	ECharacterControlType CurrentCharacterControlType;


//HUD Widget
	virtual void SetupHUDWidget(class UABHUDWidget* InUserWidget) override;
};
