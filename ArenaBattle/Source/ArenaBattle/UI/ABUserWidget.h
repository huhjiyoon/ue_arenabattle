// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ABUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class ARENABATTLE_API UABUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	FORCEINLINE void SetOwningActor(AActor* NewOnwer) { OwningActor = NewOnwer; }
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Actor")
	//Getting Actor Infomation that takes Current UserWidget
	TObjectPtr<class AActor> OwningActor;  
};
